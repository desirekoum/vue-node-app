const express = require('express');
const cors = require('cors');
const BodyParser = require('body-parser');
const app = express();
const port = 3000;

app.use(cors());
app.use(BodyParser.json());

var messages = [];

app.get('/messages', (req, res) => {
    res.send(messages);
});

app.post('/messages', (req, res) => {
    let msg = req.body;
    console.log(msg);
    messages.push(msg);
    res.json(msg);
});

app.listen(port, () => console.log("The app is up and running..."));